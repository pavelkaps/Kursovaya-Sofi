﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibraryBook
{
    /// <summary>
    /// Логика взаимодействия для AboutBook.xaml
    /// </summary>
    public partial class AboutExpense : Window
    
    {

        Expense expense;
        MainWindow main;
        public AboutExpense(Expense _expense, MainWindow _main)
        {
            InitializeComponent();
            expense = _expense;
            main = _main;
            SetExpense();
        }
        
        public void SetExpense(){
            
            TitleBox.Text = "\"" + expense.Title + "\"";
            CategoryBox.Text = expense.Category.Title;
            Sum.Text = expense.Sum.ToString() + "грн";
            id.Text = "id: " + expense.Id;
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            main.GetExpenseDb().Delete(expense.Id);
            MessageBox.Show("Запись удалена");
            main.UpdateDataGrid();
            Close();

        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            Edit form = new Edit(expense, main);
            form.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            if (form.ShowDialog() == true)
            {
                expense = form.GetExpense();
                SetExpense();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibraryBook
{
    /// <summary>
    /// Логика взаимодействия для BookOrMagazine.xaml
    /// </summary>
    public partial class Actions : Window
    {
        AddExpense AddExpenseForm;
        AddCategory AddCategoryForm;
        EditCategory EditTypeForm;
        MainWindow main;
        
        public Actions(MainWindow _main)
        {
            main = _main;
            InitializeComponent();
        }

        private void AddBook(object sender, RoutedEventArgs e)
        {
            AddExpenseForm = new AddExpense(main);
            AddExpenseForm.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            if (AddExpenseForm.ShowDialog() == true)
            {
                this.DialogResult = true;
                this.Close();
            }
            else
            {
                this.DialogResult = false;
                Close();
            }
            
        }

        private void AddType(object sender, RoutedEventArgs e)
        {
            AddCategoryForm = new AddCategory(main);
            AddCategoryForm.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            if (AddCategoryForm.ShowDialog() == true)
            {
                this.DialogResult = true;
                this.Close();
            }
            else
            {
                this.DialogResult = false;
                Close();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            EditTypeForm = new EditCategory(main);
            EditTypeForm.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            EditTypeForm.ShowDialog();
            main.UpdateDataGrid();
            Close();
        }
    }
}

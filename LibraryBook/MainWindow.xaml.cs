﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using System.Data;
using System.Collections.ObjectModel;
using System.ComponentModel;
namespace LibraryBook
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public EntityRepository<Expense> ExpenseRepository;
        public EntityRepository<Category> CategoryRepository;
        public LibraryContext LibraryContext;
        public List<Category> AllList;
        public MainWindow()
        {
            LibraryContext = new LibraryContext();
            ExpenseRepository = new EntityRepository<Expense>(LibraryContext);
            CategoryRepository = new EntityRepository<Category>(LibraryContext);

            InitializeComponent();
            LoadCategoryCombobox();
            ExpenseLoad();
        }

        private void Add(object sender, RoutedEventArgs e)
        {
            Actions form = new Actions(this);
            form.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            if (form.ShowDialog() == true)
            {
                UpdateDataGrid();
                LoadCategoryCombobox();
            }
        }

        public void LoadCategoryCombobox(){

            Category AllMenu = new Category { Title = "Все", Id = 99 };

            AllList = new List<Category>();
            AllList.Add(AllMenu);
            foreach (Category item in GetCategoryDB().Load())
            {
                AllList.Add(item);
            }
            CategoryBox.ItemsSource = AllList;
            CategoryBox.SelectedValuePath = "Id";
            CategoryBox.DisplayMemberPath = "Title";
        }

        private void Window_Load(object sender, RoutedEventArgs e)
        {

        }

        private void grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Expense path = ExpenseGrid.SelectedItem as Expense;
                AboutExpense frm = new AboutExpense(path, this);
                frm.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
                frm.ShowDialog();
                UpdateDataGrid();
            }
            catch (Exception)
            {
                MessageBox.Show("Выберите заметку", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void LoadGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = "  " + (e.Row.GetIndex() + 1) + "  ";
        }


        private void ExpenseLoad()
        {
            ExpenseGrid.ItemsSource = ExpenseRepository.Load();
            LoadCategoryCombobox();
        }


        private void EntityBox_Selected(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;
            ExpenseLoad(); 
        }

        public void UpdateDataGrid()
        {
            ExpenseGrid.ItemsSource = null;
            ExpenseLoad();
        }

        public EntityRepository<Expense> GetExpenseDb()
        {
            return ExpenseRepository;
        }

        public MainWindow GetMainWindow()
        {
            return (this);
        }

        public EntityRepository<Category> GetCategoryDB()
        {
            return CategoryRepository;
        }


        private void Seach_Click(object sender, RoutedEventArgs e)
        {
            SeachForm form = new SeachForm(this);
            form.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            form.Show();
        }

        private void TypeBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Category CategoryBook = (Category)CategoryBox.SelectedItem;
                if (CategoryBook.Id != 99) { ExpenseGrid.ItemsSource = CategoryBook.Expenses; } else { SetSourceForExpenseDataGrid(ExpenseRepository.Load()); }
            }catch(Exception ex){
            
            }
        }

        public void SetSourceForExpenseDataGrid(List<Expense> a)
        {
            ExpenseGrid.ItemsSource = null;
            ExpenseGrid.ItemsSource = a;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBook
{
   public class Expense
    {
       public int Id { get; set; }
        public string Title { get; set; }
        public DateTime? date { get; set; }
        public double Sum { get; set; }
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public Expense()
        {
        Id = 0;
        Title = "N/A";
        Sum = 2000;
        }

        public Expense(int Id, string title, int Sum)
        {
        this.Id = Id;
        this.Title = title;
        this.Sum = Sum;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibraryBook
{
    /// <summary>
    /// Логика взаимодействия для EditTypeOrGenre.xaml
    /// </summary>
    public partial class EditCategory : Window
    {
        MainWindow main;
        public EditCategory(MainWindow main)
        {
            InitializeComponent();
            this.main = main;

            ListBox.ItemsSource = null;
            ListBox.ItemsSource = main.GetCategoryDB().Load();
            ListBox.SelectedValuePath = "Id";
            ListBox.DisplayMemberPath = "Title";
            ListBox.SelectedIndex = 0;
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            Category category = (Category)ListBox.SelectedItem; 
            if (TextBox.Text != "") { 
                category.Title = TextBox.Text; 
            } else { 
                category.Title = "N/A"; 
            }; 
            main.GetCategoryDB().Update(); 
            MessageBox.Show("Категория успешно изменена", "Успешно", MessageBoxButton.OK, MessageBoxImage.Information);
            Close();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            Category category = (Category)ListBox.SelectedItem;
            main.GetCategoryDB().Delete(category.Id); 
            MessageBox.Show("Категория успешно удален", "Успешно удалено", MessageBoxButton.OK, MessageBoxImage.Information); 
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Change(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;
            ListBox.SelectedIndex = 0;
            
        }
    }
}

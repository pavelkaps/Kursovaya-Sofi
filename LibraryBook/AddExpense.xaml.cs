﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Globalization;

namespace LibraryBook
{
    /// <summary>
    /// Логика взаимодействия для AddBook.xaml
    /// </summary>
    public partial class AddExpense : Window
    {
        Expense newExpense;
        MainWindow main;
        
        public AddExpense(MainWindow m)
        {
            main = m;
            InitializeComponent();
            CategoryLoad();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            newExpense = new Expense();
            if (TitleBox.Text == "") { newExpense.Title = "N/A"; } else { newExpense.Title = TitleBox.Text; };
            if (SumBox.Text == "") { newExpense.Sum = 0; } else { newExpense.Sum = Double.Parse(SumBox.Text, CultureInfo.InvariantCulture); };
            newExpense.date = datePicker.Value;
            newExpense.Category = (Category)CategoryBox.SelectedItem;
            main.GetExpenseDb().Insert(newExpense);
            this.DialogResult = true;
            Close();
        }

        private void CategoryLoad()
        {
            CategoryBox.ItemsSource = null;
            CategoryBox.ItemsSource = main.GetCategoryDB().Load();
            CategoryBox.SelectedValuePath = "Id";
            CategoryBox.DisplayMemberPath = "Title";
            CategoryBox.SelectedIndex = 0;

        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            Close();
        }
 
    }
}

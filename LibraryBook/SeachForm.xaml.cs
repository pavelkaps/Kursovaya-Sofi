﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Globalization;

namespace LibraryBook
{
    /// <summary>
    /// Логика взаимодействия для SeachForm.xaml
    /// </summary>
    public partial class SeachForm : Window
    {
        MainWindow main;
        List<Expense> SearchExpenses;
        public SeachForm(MainWindow _main)
        {
            main = _main;
            InitializeComponent();
            ExpenseBox.SelectedIndex = 0;
        }

        private void Seach_Click(object sender, RoutedEventArgs e)
        {
            switch(searchId(1)){
                case 1: 
                SearchExpenses = main.GetExpenseDb().Load();
                foreach (Expense expense in SearchExpenses.ToArray())
                {
                if(expense.Id!=int.Parse(SearchBox.Text)) SearchExpenses.Remove(expense);
                }
                break;
                case 2: SearchExpenses = main.GetExpenseDb().Load();
                foreach (var expense in SearchExpenses.ToArray())
                {
                if(expense.Title!=SearchBox.Text)SearchExpenses.Remove(expense);
                }
                break;
                case 3: SearchExpenses = main.GetExpenseDb().Load();
                foreach (var expense in SearchExpenses.ToArray())
                {
                    if (expense.Sum != Double.Parse(SearchBox.Text, CultureInfo.InvariantCulture)) SearchExpenses.Remove(expense);
                }
                break;
            }
            main.SetSourceForExpenseDataGrid(SearchExpenses);
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Default_Click(object sender, RoutedEventArgs e)
        {
            SearchExpenses = main.GetExpenseDb().Load();
            main.SetSourceForExpenseDataGrid(SearchExpenses);
            Close();
        }


        private void SeachTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //if (!Char.IsDigit(e.Text, 0) ) e.Handled = true;
        }

        public int searchId(int id)
        {
            ComboBoxItem selectedItem = (ComboBoxItem)ExpenseBox.SelectedItem;

            if (id == 1)
            {
                if (selectedItem.Content.ToString() == "Id") { return 1; }
                if (selectedItem.Content.ToString() == "Заголовок") { return 2; }
                if (selectedItem.Content.ToString() == "Сумма") { return 3; }
            }

            return 0;
        }

        private void Enabled(object sender, DependencyPropertyChangedEventArgs e)
        {
           
        }

        private void EnabledBook(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)ExpenseBox.SelectedItem;
            SearchBox.Visibility = Visibility.Visible;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Entity;
using System.Globalization;

namespace LibraryBook
{
    /// <summary>
    /// Логика взаимодействия для Edit.xaml
    /// </summary>
    public partial class Edit : Window
    {
        Expense expense;
        MainWindow main;
        public Edit(Expense _expense, MainWindow _main)
        {
            main = _main;
            expense = _expense;
            InitializeComponent();
            SetExpenseOnForm();
        }

        public void SetExpenseOnForm(){
            TitleBox.Text = expense.Title ;
            SumBox.Text = expense.Sum.ToString();
            datePicker.Value = expense.date;
            ExpenseLoad();
        }

        private void ExpenseLoad()
        {
            CategoryBox.ItemsSource = main.GetCategoryDB().Load();
            CategoryBox.SelectedValuePath = "Id";
            CategoryBox.DisplayMemberPath = "Title";
            CategoryBox.SelectedValue = expense.Category.Id;
        }
        

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            if (TitleBox.Text == "") {  expense.Title = "N/A"; } else { expense.Title = TitleBox.Text; };
            if (SumBox.Text == "") { expense.Sum = 0; } else { expense.Sum = Double.Parse(SumBox.Text, CultureInfo.InvariantCulture); };
            expense.Category = (Category)CategoryBox.SelectedItem;
            expense.date = datePicker.Value;
            main.ExpenseRepository.Update();
            this.DialogResult = true;
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public Expense GetExpense()
        {
            return expense;
        }
    }
}
